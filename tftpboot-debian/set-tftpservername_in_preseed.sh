#!/bin/bash

# source helper functions
. ../helperfunctions.sh

# source configuration
. ../OPTIONS.conf

# source /etc/default/
. /etc/default/tftpd-hpa
# to get variable $TFTP_DIRECTORY
# configured in /etc/default/tftpd-hpa

cd $TFTP_DIRECTORY/debian-installer/amd64/preseed
for file in $(ls);
do
	# set tftp server name
	sed -e "{
		/d-i preseed\/late_command/ s/tftp01/$(hostname)/g
	}" -i ${file}
	printAndLogMessage "Set tftp server name in preseed-file: " ${TFTP_DIRECTORY}/debian-installer/amd64/preseed/${file}

done

