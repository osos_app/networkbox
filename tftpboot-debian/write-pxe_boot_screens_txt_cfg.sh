#! /bin/bash

# source helper functions
. ../helperfunctions.sh

# source configuration
. ../OPTIONS.conf

# source /etc/default/
. /etc/default/tftpd-hpa
# to get variable $TFTP_DIRECTORY
# configured in /etc/default/tftpd-hpa
FILE_PATH="${TFTP_DIRECTORY}/debian-installer/amd64/boot-screens"

printAndLogMessage "### save original ${FILE_PATH}/txt.cfg ###"
mv -v ${FILE_PATH}/txt.cfg ${FILE_PATH}/txt.cfg.original

echo "
default install

LABEL boot.local
	localboot 0
	timeout 50
	MENU DEFAULT
	MENU LABEL ^Boot from hard disk
	TEXT HELP
	Boot from the local hard drive.  
	If you are unsure, select this option.
	ENDTEXT

menu begin Unattended_Debian_12 

	menu label ^Unattended Debian 12 from $(hostname)
	MENU PASSWD ${TFTP_MENU_PASSWD}

	label Debian_12_sda_2P_1F_2F
		menu label 2 x part: p1 & p2 -> format p1 & p2
		kernel debian-installer/amd64/linux
		append auto=true priority=critical url=tftp://$(hostname)/debian-installer/amd64/preseed/preseed_uefi_2P_1F_2F.cfg vga=788 initrd=debian-installer/amd64/initrd.gz -- quiet

	label Debian_12_sda_2P_1F
		menu label 2 x part: p1 & p2 -> format ONLY p1
		kernel debian-installer/amd64/linux
		append auto=true priority=critical url=tftp://$(hostname)/debian-installer/amd64/preseed/preseed_uefi_2P_1F.cfg vga=788 initrd=debian-installer/amd64/initrd.gz -- quiet

	MENU SEPARATOR


	label mainmenu
		menu label ^Back..
		menu exit

menu end

" > ${FILE_PATH}/txt.cfg

