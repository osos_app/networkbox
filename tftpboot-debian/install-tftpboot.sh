#!/bin/bash

# source helper functions
. ../helperfunctions.sh

# source configuration
. ../OPTIONS.conf

# install isc-dhcp-server
if [ ! "$FULLINSTALL" = "true" ];
then
	echo "FULLINSTALL=false"
	apt-get -y update
fi

printAndLogStartMessage "START: INSTALLATION OF TFTP - SERVER"

printAndLogMessage "apt-get -y install tftpd-hpa"
apt-get -y install tftpd-hpa

# source /etc/default/tftpd-hpa
. /etc/default/tftpd-hpa
# to get variable ${TFTP_DIRECTORY}
# configured in /etc/default/tftpd-hpa

printAndLogStartMessage "DOWNLOAD: netboot - package from DEBIAN server"
/bin/bash wget-debian_netboot.sh

printAndLogStartMessage "WRITE: PXE Boot Menue Selection"
/bin/bash write-pxe_boot_screens_txt_cfg.sh

printAndLogStartMessage "WRITE: UEFI Boot Menue Selection"
/bin/bash write-uefi_grub_cfg.sh

printAndLogMessage "COPY PRESEED - Files"
printAndLogMessage "cp -R preseed ${TFTP_DIRECTORY}/debian-installer/amd64"
cp -R preseed ${TFTP_DIRECTORY}/debian-installer/amd64

printAndLogMessage "SET ADMIN & ADMIN-PASSWORD FOR WORKSTATIONS IN PRESEED - FILES"
/bin/bash setup-admin_account_in_preseed.sh

printAndLogMessage "SET TFTP-SERVERNAME TO GET LAUS-FILES IN PRESEED - FILES"
/bin/bash set-tftpservername_in_preseed.sh
