#! /bin/bash

# source helper functions
. ../helperfunctions.sh

# source configuration
. ../OPTIONS.conf

# source /etc/default/
. /etc/default/tftpd-hpa
# to get variable $TFTP_DIRECTORY
# configured in /etc/default/tftpd-hpa
FILE_PATH="${TFTP_DIRECTORY}/debian-installer/amd64/grub"

printAndLogMessage "### save original ${FILE_PATH}/grub.cfg ###"
mv -v ${FILE_PATH}/grub.cfg ${FILE_PATH}/grub.cfg.original

echo "
if [ x\$feature_default_font_path = xy ] ; then
   font=unicode
else
   font=\$prefix/font.pf2
fi

if loadfont \$font ; then
  set gfxmode=800x600
  set gfxpayload=keep
  insmod efi_gop
  insmod efi_uga
  insmod video_bochs
  insmod video_cirrus
  insmod gfxterm
  insmod png
  terminal_output gfxterm
fi

if background_image /isolinux/splash.png; then
  set color_normal=light-gray/black
  set color_highlight=white/black
elif background_image /splash.png; then
  set color_normal=light-gray/black
  set color_highlight=white/black
else
  set menu_color_normal=cyan/blue
  set menu_color_highlight=white/blue
fi

insmod play
play 960 440 1 0 4 440 1
submenu 'Unattended Debian 12 from $(hostname)' {
    set gfxpayload=keep
    menuentry '2 x part: p1 & p2 -> format p1 & p2' {
        linux    /debian-installer/amd64/linux append auto=true priority=critical url=tftp://$(hostname)/debian-installer/amd64/preseed/preseed_uefi_2P_1F_2F.cfg vga=788  --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry '2 x part: p1 & p2 -> format ONLY p1' {
        linux   /debian-installer/amd64/linux append auto=true priority=critical url=tftp://$(hostname)/debian-installer/amd64/preseed/preseed_uefi_2P_1F.cfg vga=788 --- quiet
        initrd  /debian-installer/amd64/initrd.gz
    }
}
submenu --hotkey=a 'Advanced options ...' {
    set gfxpayload=keep
    menuentry '... Expert install' {
        set background_color=black
        linux    /debian-installer/amd64/linux priority=low vga=788 ---
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry '... Rescue mode' {
        set background_color=black
        linux    /debian-installer/amd64/linux vga=788 rescue/enable=true --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry '... Automated install' {
        set background_color=black
        linux    /debian-installer/amd64/linux auto=true priority=critical vga=788 --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry --hotkey=x '... Expert install with speech synthesis' {
        set background_color=black
        linux    /debian-installer/amd64/linux priority=low vga=788 speakup.synth=soft ---
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry --hotkey=r '... Rescue mode with speech synthesis' {
        set background_color=black
        linux    /debian-installer/amd64/linux vga=788 rescue/enable=true speakup.synth=soft --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    menuentry --hotkey=a '... Automated install with speech synthesis' {
        set background_color=black
        linux    /debian-installer/amd64/linux auto=true priority=critical vga=788 speakup.synth=soft --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    submenu '... Desktop environment menu ...' {
        set gfxpayload=keep
        submenu '... GNOME desktop boot menu ...' {
            set gfxpayload=keep
            menuentry '... Install' {
                set background_color=black
                linux    /debian-installer/amd64/linux desktop=gnome vga=788 --- quiet
                initrd   /debian-installer/amd64/initrd.gz
            }
            submenu '... GNOME advanced options ...' {
                set gfxpayload=keep
                menuentry '... Expert install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=gnome priority=low vga=788 ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry '... Automated install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=gnome auto=true priority=critical vga=788 --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=x '... Expert install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=gnome priority=low vga=788 speakup.synth=soft ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=a '... Automated install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=gnome auto=true priority=critical vga=788 speakup.synth=soft --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
            }
        }
        submenu '... KDE Plasma desktop boot menu ...' {
            set gfxpayload=keep
            menuentry '... Install' {
                set background_color=black
                linux    /debian-installer/amd64/linux desktop=kde vga=788 --- quiet
                initrd   /debian-installer/amd64/initrd.gz
            }
            submenu '... KDE Plasma advanced options ...' {
                set gfxpayload=keep
                menuentry '... Expert install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=kde priority=low vga=788 ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry '... Automated install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=kde auto=true priority=critical vga=788 --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=x '... Expert install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=kde priority=low vga=788 speakup.synth=soft ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=a '... Automated install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=kde auto=true priority=critical vga=788 speakup.synth=soft --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
            }
        }
        submenu '... LXDE desktop boot menu ...' {
            set gfxpayload=keep
            menuentry '... Install' {
                set background_color=black
                linux    /debian-installer/amd64/linux desktop=lxde vga=788 --- quiet
                initrd   /debian-installer/amd64/initrd.gz
            }
            submenu '... LXDE advanced options ...' {
                set gfxpayload=keep
                menuentry '... Expert install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=lxde priority=low vga=788 ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry '... Automated install' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=lxde auto=true priority=critical vga=788 --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=x '... Expert install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=lxde priority=low vga=788 speakup.synth=soft ---
                    initrd   /debian-installer/amd64/initrd.gz
                }
                menuentry --hotkey=a '... Automated install with speech synthesis' {
                    set background_color=black
                    linux    /debian-installer/amd64/linux desktop=lxde auto=true priority=critical vga=788 speakup.synth=soft --- quiet
                    initrd   /debian-installer/amd64/initrd.gz
                }
            }
        }
    }
}
submenu --hotkey=d 'Accessible dark contrast installer menu ...' {
    set menu_color_normal=white/black
    set menu_color_highlight=yellow/black
    set color_normal=white/black
    set color_highlight=yellow/black
    background_image
    set gfxpayload=keep
    menuentry '... Install' {
        set background_color=black
        linux    /debian-installer/amd64/linux vga=788 theme=dark --- quiet
        initrd   /debian-installer/amd64/initrd.gz
    }
    submenu --hotkey=a '... Advanced options ...' {
        set menu_color_normal=white/black
        set menu_color_highlight=yellow/black
        set color_normal=white/black
        set color_highlight=yellow/black
        background_image
        set gfxpayload=keep
        menuentry '... Expert install' {
            set background_color=black
            linux    /debian-installer/amd64/linux priority=low vga=788 theme=dark ---
            initrd   /debian-installer/amd64/initrd.gz
        }
        menuentry '... Rescue mode' {
            set background_color=black
            linux    /debian-installer/amd64/linux vga=788 rescue/enable=true theme=dark --- quiet
            initrd   /debian-installer/amd64/initrd.gz
        }
        menuentry '... Automated install' {
            set background_color=black
            linux    /debian-installer/amd64/linux auto=true priority=critical vga=788 theme=dark --- quiet
            initrd   /debian-installer/amd64/initrd.gz
        }
    }
}

" > ${FILE_PATH}/grub.cfg

