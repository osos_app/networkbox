#! /bin/bash

# source helper functions
. ../helperfunctions.sh

# source configuration
. ../OPTIONS.conf

# source /etc/default/
. /etc/default/tftpd-hpa
# to get variable $TFTP_DIRECTORY
# configured in /etc/default/tftpd-hpa

cd ${TFTP_DIRECTORY}

printAndLogMessage "### Download ${NETBOOT_TAR_FILE} from ${NETBOOT_SOURCE_PATH} ###"
printAndLogMessage "wget -P ${TFTP_DIRECTORY} ${NETBOOT_SOURCE_PATH}/${NETBOOT_TAR_FILE}"
wget -P ${TFTP_DIRECTORY} ${NETBOOT_SOURCE_PATH}/${NETBOOT_TAR_FILE}

printAndLogMessage "### extract ${NETBOOT_TAR_FILE}"
printAndLogMessage "tar -xf ${NETBOOT_TAR_FILE}"
tar -xf ${NETBOOT_TAR_FILE}

