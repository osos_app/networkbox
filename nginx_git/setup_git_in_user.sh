#!/bin/bash

# create git repository in /home/user
git init

# make empty folders commit-able
touch Bilder/.gitkeep
touch Dokumente/.gitkeep
touch Downloads/.gitkeep
touch Öffentlich/.gitkeep
touch Schreibtisch/.gitkeep
touch Videos/.gitkeep
touch Vorlagen/.gitkeep
touch Musik/.gitkeep

# create a .gitconfig file
echo "
[user]
    name = Statndard User APP
    email = edv.app@tsn.at

[alias]
    hist = log --oneline --decorate --graph
" > .gitconfig

# create a .gitignore file
echo "
.cache
" > .gitignore

echo "CHECK with >>git status" if unnecessary files are added"
echo "and block them in .gitignore"