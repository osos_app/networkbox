#!/bin/bash

########################################################################
#
# from:
# https://www.howtoforge.com/tutorial/ubuntu-git-server-installation/
# https://stackoverflow.com/questions/18673552/setting-up-read-only-http-access-to-git-repo/40313590#40313590
#
########################################################################

GIT_DIR="repositories"
GIT_ROOT="/var/www/html/${GIT_DIR}"
GIT_REPOSITORY="user.save.git"
GIT_USER="user"
SERVER_NAME="git01"

# update
apt-get -y update

# install personal packages
apt-get install net-tools mc

# install packages for nginx & git
apt-get -y install nginx git fcgiwrap apache2-utils unzip 

# create directory for user.save git repository
mkdir -p ${GIT_ROOT}

# create bare git repository
cd ${GIT_ROOT}
mkdir ${GIT_REPOSITORY}
cd ${GIT_REPOSITORY}
git --bare init 

# rename branch from master to main 
git branch -m main

# update the Git server information 
# https://git-scm.com/docs/git-update-server-info
git update-server-info

# change the ownership of repositories
chown -R -v www-data:www-data ${GIT_ROOT}

# create a user: "user.save" and set a password: 
htpasswd -c ${GIT_ROOT}/.htpasswd ${GIT_USER}

echo "
server {
        listen 80;

        root ${GIT_ROOT};

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name ${SERVER_NAME};
        
        client_max_body_size 0;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files \$uri \$uri/ =404;
        }

        location ~ (/.*) {
                if ( \$arg_service = git-receive-pack) {
                        rewrite (/.*) /git_write\$1 last;
                }

                if (\$uri ~ (.*/git-receive-pack$)) {
                        rewrite (/.*) /git_write\$1 last;
                }

                if (\$arg_service = git-upload-pack) {
                        rewrite (/.*) /git_read\$1 last;
                }

                if (\$uri ~ (.*/git-upload-pack$)) {
                        rewrite (/.*) /git_read\$1 last;
                }

                location ~ /git_read(/.*) {
                    include /etc/nginx/fastcgi_params;
                    fastcgi_param SCRIPT_FILENAME /usr/lib/git-core/git-http-backend;
                    fastcgi_param GIT_HTTP_EXPORT_ALL \"true\";
                    fastcgi_param GIT_PROJECT_ROOT ${GIT_ROOT};
                    fastcgi_param REMOTE_USER \$remote_user;
                    fastcgi_param PATH_INFO \$1;
                    fastcgi_pass  unix:/var/run/fcgiwrap.socket;
                }

                location ~ /git_write(/.*) {
                    auth_basic \"Git Login\";
                    auth_basic_user_file \"${GIT_ROOT}/.htpasswd\";

                    include /etc/nginx/fastcgi_params;
                    fastcgi_param SCRIPT_FILENAME /usr/lib/git-core/git-http-backend;
                    fastcgi_param GIT_HTTP_EXPORT_ALL \"true\";
                    fastcgi_param GIT_PROJECT_ROOT ${GIT_ROOT};
                    fastcgi_param REMOTE_USER \$remote_user;
                    fastcgi_param PATH_INFO \$1;
                    fastcgi_pass  unix:/var/run/fcgiwrap.socket;
                }
        }
}" > /etc/nginx/conf.d/${GIT_DIR}.conf

# verify nginx setup
nginx -t

# restart
systemctl restart nginx
