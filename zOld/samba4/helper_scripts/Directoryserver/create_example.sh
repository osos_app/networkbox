#!/bin/bash


## toplevel ou, where all the stuff lives
TOP_OU="COMPANY"
## 
## second level OU number 1
SECOND_LEVEL_01="Office01"
## primary linux group, where all users from SECOND_LEVEL_01 belong too
PRIMARY_LINUX_GROUP_NAME_01="Office01"
PRIMARY_LINUX_GROUP_UIDNUMBER_01="4000001"
##
## second level OU number 2
SECOND_LEVEL_02="Office02"
## primary linux group, where all users from SECOND_LEVEL_01 belong too
PRIMARY_LINUX_GROUP_NAME_02="Office02"
PRIMARY_LINUX_GROUP_UIDNUMBER_02="5000001"


## create ou for Office01
samba-tool ou add OU=${SECOND_LEVEL_01},OU=${TOP_OU}

## create ou for users
samba-tool ou add OU=Users,OU=${SECOND_LEVEL_01},OU=${TOP_OU}

## create ou for groups
samba-tool ou add OU=Groups,OU=${SECOND_LEVEL_01},OU=${TOP_OU}

## create primary linux group for Office01 with unused nis-domain
samba-tool group add ${PRIMARY_LINUX_GROUP_NAME_01} --groupou=OU=Groups,OU=${SECOND_LEVEL_01},OU=${TOP_OU} --gid-number=${PRIMARY_LINUX_GROUP_UIDNUMBER_01} --nis-domain="NISDOMAIN"


## create ou for Office02
samba-tool ou add OU=${SECOND_LEVEL_02},OU=${TOP_OU}

## create ou for users
samba-tool ou add OU=Users,OU=${SECOND_LEVEL_02},OU=${TOP_OU}

## create ou for groups
samba-tool ou add OU=Groups,OU=${SECOND_LEVEL_02},OU=${TOP_OU}

## create primary linux group for Office01 with unused nis-domain
samba-tool group add ${PRIMARY_LINUX_GROUP_NAME_02} --groupou=OU=Groups,OU=${SECOND_LEVEL_02},OU=${TOP_OU} --gid-number=${PRIMARY_LINUX_GROUP_UIDNUMBER_02} --nis-domain="NISDOMAIN"


## add users from csv file
/bin/bash add_users_from_csv_file.sh csv/office_users.csv

## add hosts from csv file
/bin/bash add_hosts_from_csv_file.sh csv/office_hosts.csv

