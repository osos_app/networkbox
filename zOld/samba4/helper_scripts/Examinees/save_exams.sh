#!/bin/bash

# should run from directory e/zzzOLD
# because of script: check-for_unused_homedirs.sh

# change to directory where all examinee_XXXXXX_XXX live
cd ..

DATESTAMP=$(date +%Y_%m_%d)
ARCHIVE_DIR=archive_${DATESTAMP}

# create directory to save exams
mkdir -v ${ARCHIVE_DIR}

for EXAMINEE in $(ls);
do
	if [[ -d ${EXAMINEE} && ${EXAMINEE} == examinee* ]];
	then 
		echo "# checking ${EXAMINEE} for exams"
		EXAMINEE_CONTENT=$(ls ${EXAMINEE})
		if [[ ! ${EXAMINEE_CONTENT} == "" ]];
		then
			cp -R -v ${EXAMINEE} ${ARCHIVE_DIR}
			rm -R -v ${EXAMINEE}/*
		fi 
	fi
done

tar -czvf archive.tar.gz ${ARCHIVE_DIR}
TIMESTAMP=$(date +%Y_%m_%d_%T)
mv -v archive.tar.gz zzzOLD/z_archive_${TIMESTAMP}.tar.gz
rm -R ${ARCHIVE_DIR}
tree

