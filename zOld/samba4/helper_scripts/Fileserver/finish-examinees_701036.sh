#!/bin/bash

## 701036
echo "finish examinees for 701036"
cd /home/users/701036/e

## set group for examinee_NNNNNN_XXX to school-lehrersnr701036
## so ONLY teachers can read all examinee_NNNNNN_XXX 
## for security checks
echo "set examinee_NNNNNN_XXX to school-lehrersnr701036"
chgrp -v school-lehrersnr701036 examinee*

## set mode for examinee_NNNNNN_XXX to 750
chmod -v 750 examinee*

## create dir for templates
mkdir -v Vorlagen

## set group for Vorlagen to school-lehrerexamineesnr701036
chgrp -v school-lehrerexamineesnr701036 Vorlagen

## set mode for Vorlagen 750
## ONLY teachers and examinees can change into Vorlagen
## BUT DO NOT HAVE create - rights
chmod -v 750 Vorlagen

## therefore we build structure where teachers can create templates
## we do this subject per subject
cd Vorlagen
for subject in AlleAnderen Biologie Deutsch DG Englisch Franzoesisch Italienisch Mathematik Physik Russisch;
do
	mkdir ${subject}
	chgrp -v school-lehrersnr701036 ${subject}
	## teacher can do ANYTHING, examinees can READ
	chmod -v 775 ${subject}
	## 775 SGID Bit ensures, that new files will belong to group school-lehrersnr701036
	chmod -v g+s ${subject}
done


